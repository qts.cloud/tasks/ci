# gitlab-runner

1. Create Manifests

    * Using [GitOps Setup](https://docs.gitlab.com/runner/install/kubernetes-agent.html#example-runner-manifest) for deploying gitlab-runner

      ```sh
      #? https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml
      cat <<END > /tmp/override.values.yml
      gitlabUrl: https://gitlab.com/
      rbac:
        create: true
        clusterWideAccess: true
      runners:
          privileged: true
      END
      GITLAB_NAMESPACE="ci"
      helm template --include-crds --namespace ${GITLAB_NAMESPACE} gitlab-runner -f /tmp/override.values.yml gitlab/gitlab-runner > runner-manifest.yaml
      ```

1. Update Namespace

    * Add/Update namespace for all resources

1. Update secrets

    ```sh
    # Generate Secret and encrypt it.
    echo -n "secret-token" | kubectl create secret generic gitlab-runner -n ${GITLAB_NAMESPACE} --dry-run=client --type=Opaque --from-file=runner-registration-token=/dev/stdin --from-literal=runner-token="" -o json > my-secret.json
    kubeseal --format=json --cert=../sealed-secrets/sealed-secrets.pub.pem < my-secret.json > sealed-secret.enc.json
    ```
