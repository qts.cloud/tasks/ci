# Bitnami Sealed Secrets

## Prequisites

```sh
brew install kubeseal
```

## How to Install

```sh
#? Download latest `controller.yaml` from github
curl -fsSL https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.5/controller.yaml -O

#? git commit & push
git add .
git commit -m"feat(sealed-secrets): add controller"
git push

#? Wait until it gets deployed by Gitlab Agent
kubectl get pods -n kube-system -l name=sealed-secrets-controller
```

## How to Use

```sh
#? Store public certificate
kubeseal --fetch-cert > sealed-secrets.pub.pem

#? Generate Kubernetes Secret
echo -n "Very secret" | kubectl create secret generic my-secret -n my-namespace --dry-run=client --type=Opaque --from-file=token=/dev/stdin -o yaml > my-secret.yml
kubeseal --format=yaml --scope cluster-wide --cert=sealed-secrets.pub.pem < my-secret.yml > sealed-secret.yaml
```
