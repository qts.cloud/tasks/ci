# gitlab-runner

1. Generate Manifests

      ```sh
      #? https://cert-manager.io/docs/installation/helm/

      #? Add Helm Repository
      $ helm repo add jetstack https://charts.jetstack.io
      $ helm repo update

      #? Set Overrides
      # https://artifacthub.io/packages/helm/cert-manager/cert-manager
      cat <<END > /tmp/override.values.yml
      installCRDs: true
      END
      
      NAMESPACE="ci"
      helm template --include-crds --namespace ${NAMESPACE} cert-manager -f /tmp/override.values.yml jetstack/cert-manager > manifest_1.yaml
      ```

1. Update secrets

    ```sh
    # Generate Secret and encrypt it.
    echo -n "secret-token" | kubectl create secret generic cloudflare-api-token-secret -n ${NAMESPACE} --dry-run=client --type=Opaque --from-file=api-token=/dev/stdin -o json > my-secret.json
    kubeseal --format=json --cert=../sealed-secrets/sealed-secrets.pub.pem < my-secret.json > sealed-secret.enc.json
    ```
